# Cmake arguments: TARGET_ARCH_32, TARGET_ARCH_64
#
# Host: HOST_OS_ARCH_32 or HOST_OS_ARCH_64
# Target: TARGET_OS_ARCH_32 or TARGET_OS_ARCH_64
#
# HOST_OS_ARCH: set to a normalized name: X86 or X64
# TARGET_OS_ARCH: set to a normalized name: X86 or X64

# See https://github.com/axr/solar-cmake/blob/master/TargetArch.cmake
include(CheckSymbolExists)

# X86
check_symbol_exists("_M_IX86"   ""  T_M_IX86)
check_symbol_exists("__i386__"  ""  T_I386)
if(T_M_IX86 OR T_I386)
    option(host_os_supported_bit32 "Host system support 32-bits." ON)
endif()

# X86_64
check_symbol_exists("_M_AMD64"    ""  T_M_AMD64)
check_symbol_exists("__x86_64__"  ""  T_X86_64)
check_symbol_exists("__amd64__"   ""  T_AMD64)

if(T_M_AMD64 OR T_X86_64 OR T_AMD64)
    option(host_os_supported_bit64 "Host system support 64-bits." ON)
endif()

if(CMAKE_SIZEOF_VOID_P EQUAL 8 AND host_os_supported_bit64)
    option(HOST_OS_ARCH_64 "Host System is 64-bits." ON)
    set(HOST_OS_ARCH "x64" CACHE INTERNAL "Host OS Arch" FORCE)
elseif(CMAKE_SIZEOF_VOID_P EQUAL 4 AND host_os_supported_bit32)
    option(HOST_OS_ARCH_32 "Host System is 32-bits." ON)
    set(HOST_OS_ARCH "x86" CACHE INTERNAL "Host OS Arch" FORCE)
else()
    message(FATAL_ERROR "Unknown host os architecture!")
endif()

# Check if do cross compiling, if not just following the host
if(TARGET_ARCH_32 AND TARGET_ARCH_64)
    set(emsg "'TARGET_ARCH_32' and 'TARGET_ARCH_64' are mutually exclusive!")
    message(FATAL_ERROR "${emsg}")
elseif(TARGET_ARCH_32 AND NOT TARGET_ARCH_64)
    option(TARGET_OS_ARCH_32 "Target System is 32-bits." ON)
    set(TARGET_OS_ARCH "x86" CACHE INTERNAL "Target OS Arch" FORCE)
elseif(NOT TARGET_ARCH_32 AND TARGET_ARCH_64)
    option(TARGET_OS_ARCH_64 "Target System is 64-bits." ON)
    set(TARGET_OS_ARCH "x64" CACHE INTERNAL "Target OS Arch" FORCE)
else()
    if(HOST_OS_ARCH_32)
        option(TARGET_OS_ARCH_32 "Target System is 32-bits." ON)
        set(TARGET_OS_ARCH "x86" CACHE INTERNAL "Target OS Arch" FORCE)
    else()
        option(TARGET_OS_ARCH_64 "Target System is 64-bits." ON)
        set(TARGET_OS_ARCH "x64" CACHE INTERNAL "Target OS Arch" FORCE)
    endif()
endif()
