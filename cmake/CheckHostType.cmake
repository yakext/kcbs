# HOST_OS: windows, linux, macos
# HOST_OS_WINDOWS, HOST_OS_LINUX, HOST_OS_MACOSX

# Check host build: Windows
if(CMAKE_HOST_WIN32 AND CMAKE_HOST_SYSTEM_NAME MATCHES "Windows")
    set(HOST_OS "windows" CACHE INTERNAL "Host OS Type" FORCE)
    option(HOST_OS_WINDOWS "Host System: Windows" ON)
    option(CURRENT_HOST_SUPPORTED "Current host is supported." ON)
endif()

# Check host build: UNIX
if(CMAKE_HOST_UNIX AND CMAKE_HOST_SYSTEM_NAME MATCHES "Linux")
    # Check UNIX first, because Macos is a kinde of UNIX
    set(HOST_OS "linux" CACHE INTERNAL "Host OS Type" FORCE)
    option(HOST_OS_LINUX "Build for Linux or Linux like operating systems." ON)
    option(CURRENT_HOST_SUPPORTED "Current host is supported" ON)
endif()

# Check host build: MacOSX
if(CMAKE_HOST_APPLE AND CMAKE_HOST_SYSTEM_NAME MATCHES "Darwin")
    set(HOST_OS "macos" CACHE INTERNAL "Host OS Type" FORCE)
    option(HOST_OS_MACOSX "Host System: MacOSX" ON)
    option(CURRENT_HOST_SUPPORTED "Current host is supported." ON)
endif()

if(NOT CURRENT_HOST_SUPPORTED)
    set(err_msg "Not unsupported host: ${CMAKE_HOST_SYSTEM}")
    message(FATAL_ERROR "${err_msg}")
endif()
