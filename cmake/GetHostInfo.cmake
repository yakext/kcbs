# The following will be set:
# - HOST_OS                 One of 'windows', 'linux' or 'macos'
# - HOST_OS_LINUX           True if building host is Linux
# - HOST_OS_MACOSX          True if building host is MacOS
# - HOST_OS_WINDOWS         True if building host is Windows
#
# - HOST_OS_ARCH            One of 'X86' or 'X64'
# - HOST_OS_ARCH_32         True if building host is 32-bits
# - HOST_OS_ARCH_64         True if building host is 64-bits
#
# - TARGET_OS_ARCH          One of 'X86' or 'X64'
# - TARGET_OS_ARCH_32       True if building target is 32-bits
# - TARGET_OS_ARCH_64       True if building target is 64-bits
#
# - CURRENT_TIMESTAMP       Current timestamp
# - HOST_NAME               Host name
# - HOST_USER_NAME          Host user name
# - HOST_SYSTEM_NAME        Host system name
# - HOST_SYSTEM_VERSION     Host system version
#
# - HOST_ENDIAN             One of 'little endian' or 'big endian'
# - HOST_ENDIAN_L           True if host is little endian
# - HOST_ENDIAN_B           True if host is big endian

include(CheckHostType)
include(CheckHostArch)
include(CheckHostInfo)
include(CheckHostEndian)

GetCurrentSystemTime(CURRENT_TIMESTAMP)
GetHostNameUserName(HOST_USER_NAME HOST_NAME)
GetHostSystemInfo(HOST_SYSTEM_NAME HOST_SYSTEM_VERSION)

GetHostEndian(HOST_ENDIAN)
if(HOST_ENDIAN STREQUAL "little endian")
    option(HOST_ENDIAN_L "Host system little endian" ON)
elseif(HOST_ENDIAN STREQUAL "big endian")
    option(HOST_ENDIAN_B "Host system big endian" ON)
endif()

set(KEEP_QUIET True CACHE INTERNAL "Keep quiet")
if(NOT KEEP_QUIET)
    message(STATUS "-------------------------------------------")
    message(STATUS "Host System: ${HOST_OS}")
    message(STATUS "Is Linux: ${HOST_OS_LINUX}")
    message(STATUS "Is MacOSX: ${HOST_OS_MACOSX}")
    message(STATUS "Is Windows: ${HOST_OS_WINDOWS}")

    message(STATUS "Host Arch: ${HOST_OS_ARCH}")
    message(STATUS "Is host 32-bits: ${HOST_OS_ARCH_32}")
    message(STATUS "Is host 64-bits: ${HOST_OS_ARCH_64}")

    message(STATUS "Target Arch: ${TARGET_OS_ARCH}")
    message(STATUS "Is target 32-bits: ${TARGET_OS_ARCH_32}")
    message(STATUS "Is target 64-bits: ${TARGET_OS_ARCH_64}")

    message(STATUS "Host Endian: ${HOST_ENDIAN}")
    message(STATUS "Is host big endian: ${HOST_ENDIAN_B}")
    message(STATUS "Is host little endian: ${HOST_ENDIAN_L}")

    message(STATUS "Current timestamp: ${CURRENT_TIMESTAMP}")
    message(STATUS "Host name: ${HOST_NAME}")
    message(STATUS "Host user name: ${HOST_USER_NAME}")
    message(STATUS "Host system name: ${HOST_SYSTEM_NAME}")
    message(STATUS "Host system version: ${HOST_SYSTEM_VERSION}")
    message(STATUS "-------------------------------------------")
endif()