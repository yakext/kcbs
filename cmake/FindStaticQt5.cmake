# Search for static Qt5 libraries
#
# Link sequence is much more important

macro(PrepareStaticLibrarySearch name)
    list(INSERT search_lib_names 0
        ${CMAKE_STATIC_LIBRARY_PREFIX}${name}${CMAKE_STATIC_LIBRARY_SUFFIX})
endmacro()

macro(PrepareSharedLibrarySearch name)
    list(INSERT search_lib_names 0
        ${CMAKE_SHARED_LIBRARY_PREFIX}${name}${CMAKE_SHARED_LIBRARY_SUFFIX})
endmacro()

macro(CheckSearchResult)
    if(${ARGC} AND EXISTS ${ARGV0})
        list(APPEND QT5_LINK_LIBRARIES ${ARGV0})
    else()
        set(emsg "Do not find library: ${search_lib_names}")
        message(FATAL_ERROR ${emsg})
    endif()
endmacro()

macro(StaticQt5Search name hint_path)
    set(search_lib_names)
    PrepareStaticLibrarySearch(${name})
    find_library(QT5_STATIC_LIB_${name}
                 NAMES ${search_lib_names} NAMES_PER_DIR
                 PATHS ${QT5_INSTALL_PREFIX}/${hint_path}
                 NO_CMAKE_PATH
                 NO_DEFAULT_PATH
                 NO_CMAKE_ENVIRONMENT_PATH
                 NO_SYSTEM_ENVIRONMENT_PATH
                 NO_CMAKE_SYSTEM_PATH)
    CheckSearchResult(${QT5_STATIC_LIB_${name}})
endmacro()

# Static build Qt5 bundled library: plugins/platforms/libqxcb.a
StaticQt5Search("qxcb" "plugins/platforms")
# Static build Qt5 bundled library: lib/libQt5XcbQpa.a
StaticQt5Search("Qt5XcbQpa" "lib")
# Static build Qt5 bundled library: lib/libQt5ServiceSupport.a
StaticQt5Search("Qt5ServiceSupport" "lib")
# Static build Qt5 bundled library: lib/libQt5ThemeSupport.a
StaticQt5Search("Qt5ThemeSupport" "lib")
# Static build Qt5 bundled library: lib/libQt5DBus.a
StaticQt5Search("Qt5DBus" "lib")
# Static build Qt5 bundled library: lib/libQt5EventDispatcherSupport.a
StaticQt5Search("Qt5EventDispatcherSupport" "lib")
# Static build Qt5 bundled library: lib/libQt5FontDatabaseSupport.a
StaticQt5Search("Qt5FontDatabaseSupport" "lib")

# See for details:
# https://wiki.ubuntu.com/MultiarchSpec
# https://err.no/debian/amd64-multiarch-3
if(HOST_OS_ARCH_64)
    list(APPEND multiarch_seeks_dirs "/lib64")
    list(APPEND multiarch_seeks_dirs "/usr/lib64")
    list(APPEND multiarch_seeks_dirs "/lib/x86_64-linux-gnu")
    list(APPEND multiarch_seeks_dirs "/usr/lib/x86_64-linux-gnu")
else()
    list(APPEND multiarch_seeks_dirs "/lib32")
    list(APPEND multiarch_seeks_dirs "/usr/lib32")
    list(APPEND multiarch_seeks_dirs "/lib/i386-linux-gnu")
    list(APPEND multiarch_seeks_dirs "/usr/lib/i386-linux-gnu")
endif()

macro(SystemLibrarySearch)
    set(search_lib_names ${ARGV0})
    PrepareSharedLibrarySearch(${ARGV0}) # shared libray come first
    if(${ARGC} EQUAL 1)
        PrepareStaticLibrarySearch(${ARGV0}) # static libray come first
    endif()
    find_library(SYS_LIB_${ARGV0}
        NAMES ${search_lib_names} NAMES_PER_DIR
        PATHS ${multiarch_seeks_dirs})
    CheckSearchResult(${SYS_LIB_${ARGV0}})
endmacro()

# static build Qt5 system library: libfontconfig.a, libfontconfig.so, fontconfig
SystemLibrarySearch("fontconfig")
# static build Qt5 system library: libexpat.a, libexpat.so, expat
SystemLibrarySearch("expat")
# static build Qt5 system library: libfreetype.a, libfreetype.so, freetype
SystemLibrarySearch("freetype")

# The X.Org project provides an open source implementation of the
# X Window System, which is widely used in linux system, so just
# link to the dynamic libraries.
#
# FindX11.cmake: Find X11 installation
#
# X11_FOUND        - True if X11 is available
# X11_INCLUDE_DIR  - include directories to use X11
# X11_LIBRARIES    - link against these to use X11
find_package(X11 REQUIRED)
# libXi.so: library for the X Input Extension
# libXau.so: A Sample Authorization Protocol for X
set(search_lib_names "libXi.so")
CheckSearchResult(${X11_Xi_LIB})
# libX11.so: library for the X Window System
set(search_lib_names "libX11.so")
CheckSearchResult(${X11_X11_LIB})

# Xlib/XCB interface library
#
# Provides functions needed by clients which take advantage of
# Xlib/XCB to mix calls to both Xlib and XCB over the same X connection.
#
# static build Qt5 system library: libX11-xcb.a, libX11-xcb.so, X11-xcb
SystemLibrarySearch("X11-xcb")

# static build Qt5 bundled library: lib/libxcb-static.a
StaticQt5Search("xcb-static" "lib")

# The X protocol C-language Binding (XCB) is a replacement for Xlib featuring
# a small footprint, latency hiding, direct access to the protocol, improved
# threading support, and extensibility. On Linux, the xcb QPA
# (Qt Platform Abstraction) platform plugin is used.
#
# static build Qt5 system library: libxcb.so, libxcb.a
SystemLibrarySearch("xcb" ON)

# static build Qt5 bundled library: plugins/imageformats/libqgif.a
StaticQt5Search("qgif" "plugins/imageformats")
# static build Qt5 bundled library: plugins/imageformats/libqicns.a
StaticQt5Search("qicns" "plugins/imageformats")
# static build Qt5 bundled library: plugins/imageformats/libqico.a
StaticQt5Search("qico" "plugins/imageformats")
# static build Qt5 bundled library: plugins/imageformats/libqjpeg.a
StaticQt5Search("qjpeg" "plugins/imageformats")
# static build Qt5 bundled library: plugins/imageformats/libqtga.a
StaticQt5Search("qtga" "plugins/imageformats")
# static build Qt5 bundled library: plugins/imageformats/libqtiff.a
StaticQt5Search("qtiff" "plugins/imageformats")
# static build Qt5 bundled library: plugins/imageformats/libqwbmp.a
StaticQt5Search("qwbmp" "plugins/imageformats")
# static build Qt5 bundled library: plugins/imageformats/libqwebp.a
StaticQt5Search("qwebp" "plugins/imageformats")
# static build Qt5 bundled library: lib/libQt5Widgets.a
StaticQt5Search("Qt5Widgets" "lib")
# static build Qt5 bundled library: lib/libQt5Gui.a
StaticQt5Search("Qt5Gui" "lib")

# static build Qt5 system library: libpng12.a, libpng.a, libpng12.so, libpng.so
SystemLibrarySearch("png")
SystemLibrarySearch("png12")

# static build Qt5 bundled library: lib/libqtharfbuzz.a
StaticQt5Search("qtharfbuzz" "lib")
# static build Qt5 bundled library: lib/libQt5Core.a
StaticQt5Search("Qt5Core" "lib")

# ICU - International Components for Unicode
# if use the static ICU, the output will get bigger nearly double size.
#
# static build Qt5 system library: libicui18n.a, libicui18n.so
if(USE_STATIC_LIB_ICU)
    SystemLibrarySearch("icui18n")
else()
    SystemLibrarySearch("icui18n" ON)
endif()

# static build Qt5 system library: libicuuc.a, libicuuc.so
if(USE_STATIC_LIB_ICU)
    SystemLibrarySearch("icuuc")
else()
    SystemLibrarySearch("icuuc" ON)
endif()

# static build Qt5 system library: libicudata.a, libicudata.so
if(USE_STATIC_LIB_ICU)
    SystemLibrarySearch("icudata")
else()
    SystemLibrarySearch("icudata" ON)
endif()

# static build Qt5 system library: libm.so, libm.a
SystemLibrarySearch("m" ON)
# static build Qt5 system library: libz.so, libz.a
SystemLibrarySearch("z" ON)
# static build Qt5 bundled library: lib/libqtpcre2.a
StaticQt5Search("qtpcre2" "lib")
# static build Qt5 system library: libdl.so, libdl.a
SystemLibrarySearch("dl" ON)

# static build Qt5 system library: libgthread-2.0.so, libgthread-2.0.a
# Gthread: part of Glib
# Pthread: POSIX thread standard
SystemLibrarySearch("gthread-2.0" ON)

# The GLib provides the core application building blocks for libraries and
# applications written in C.
#
# It contains low-level libraries useful for providing data structure handling
# for C, portability wrappers and interfaces for such runtime functionality as
# an event loop, threads, dynamic loading and an object system.
SystemLibrarySearch("glib-2.0" ON)
