#!/usr/bin/env bash

# This should be invocated at the root directory

set -e

THIS_FILE="${0##*/}"
THIS_DIR="$(cd $(dirname $0); pwd; cd - > /dev/null)"
REPO_DIR="$(cd ${THIS_DIR}/..; pwd; cd - > /dev/null)"

# The location of the Linux kernel source tree
K_DIR="$(cd $1; pwd; cd - > /dev/null)"
if [ ! \( -n "${K_DIR}" -a -d "${K_DIR}/kernel" \) ]; then
    if [ -n "${K_DIR}" ]; then
        printf "%s: not a Linux kernel source tree\n" "${K_DIR}"
    else
        printf "Usage:\n\t%s /path/to/kernel/dir\n" "${THIS_FILE}"
    fi
    exit 1
fi

# Save current version
K_HASH_OLD=$( head -n 1 .version | awk '{ print $(2); }' )

# Get the kernel version
eval $(head -n 6 "${K_DIR}/Makefile" | tail -n 5 \
     | sed -e 's/^/K_/; s/"//g; s/ = \{0,1\}/="/; s/$/"/;')
K_HASH_NEW="$(cd "${K_DIR}"; git log -n 1 --pretty='format:%H')"
printf "Found Linux kernel %d.%d.%d%s '%s' (%7.7s)\n" \
       "${K_VERSION}" "${K_PATCHLEVEL}" "${K_SUBLEVEL}" \
       "${K_EXTRAVERSION}" "${K_NAME}" "${K_HASH_NEW}"

K_TWEAK=$(echo "${K_EXTRAVERSION}" | sed 's/-//')
sed -ri "s/^(set\(KCONFIG_VERSION_MAJOR\s*)[0-9]*(\))$/\1${K_VERSION}\2/" ${REPO_DIR}/CMakeLists.txt
sed -ri "s/^(set\(KCONFIG_VERSION_MINOR\s*)[0-9]*(\))$/\1${K_PATCHLEVEL}\2/" ${REPO_DIR}/CMakeLists.txt
sed -ri "s/^(set\(KCONFIG_VERSION_PATCH\s*)[0-9]*(\))$/\1${K_SUBLEVEL}\2/" ${REPO_DIR}/CMakeLists.txt
sed -ri "s/^(set\(KCONFIG_VERSION_TWEAK\s*)[0-9]*(\))$/\1${K_TWEAK}\2/" ${REPO_DIR}/CMakeLists.txt

KERNEL_VERSION="${K_VERSION}.${K_PATCHLEVEL}.${K_SUBLEVEL}${K_EXTRAVERSION}"

# Store the new version
printf "%s %s %s" "${KERNEL_VERSION}" "${K_HASH_NEW}" "${K_NAME}" > .version

# Sync the kconfig files
K_FILES=""
while read k_file x_separator x_file; do
    if [ -z "${k_file}" -a -z "${x_separator}" -a -z "${x_file}" ]; then
        continue;
    fi

    K_FILES="${K_FILES} ${k_file}"
    mkdir -p "${x_file%/*}"
    cp -v "${K_DIR}/${k_file}" "${x_file}"
    if [ -f "${x_file}.patch" ]; then
        patch --no-backup-if-mismatch -g0 -F1 -p1 -f < "${x_file}.patch"
    fi
done < scripts/ksync.list

# Save the changelog between the old cset and now
printf "Sync kconfig changes:\n"
( cd "${K_DIR}"
  git log --no-merges --pretty='tformat:%h %s' \
    "${K_HASH_OLD}..${K_HASH_NEW}" ${K_FILES} \
) | tac \
  | tee -a "scripts/ksync.log" \
  | sed -e 's/^/    /;'

# Update modules/parser/zconf.c
${THIS_DIR}/auto/zconf.sh ${THIS_DIR}/tmp ${REPO_DIR}/modules/parser
