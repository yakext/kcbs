#!/usr/bin/env bash

THIS_DIR="$(cd $(dirname $0); pwd; cd - > /dev/null)"

WORKING_DIR="$(cd $1; pwd; cd - > /dev/null)"
OUTPUT_DIR="$(cd $2; pwd; cd - > /dev/null)"

INPUT_FILE=${WORKING_DIR}/zconf.y
OUTPUT_SOURCE=${OUTPUT_DIR}/zconf.c
OUTPUT_HEADER=$(echo ${OUTPUT_SOURCE} | sed -e s/cc$/hh/ -e s/cpp$/hpp/ -e s/cxx$/hxx/ -e s/c++$/h++/ -e s/c$/h/)

${THIS_DIR}/ylwrap ${INPUT_FILE} \
    y.tab.c ${OUTPUT_SOURCE} \
    y.tab.h ${OUTPUT_HEADER} \
    y.output ${WORKING_DIR}/yconf.output \
    -- bison -y -t -l
