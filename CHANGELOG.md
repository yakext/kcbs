# Change Log
ALL NOTABLE CHANGES WILL BE DOCUMENTED HERE.

## Release [v0.0.1](https://gitlab.com/charlie-wong/kcbs/releases/tag/v0.0.1)

### BugFixes
- fix: bug fix & config building ([3874715](https://gitlab.com/charlie-wong/kcbs/commit/3874715))
- fix: kconfig root menu can be custom set ([a444200](https://gitlab.com/charlie-wong/kcbs/commit/a444200))
- fix: kconfig static/shared build config ([d7bb4ee](https://gitlab.com/charlie-wong/kcbs/commit/d7bb4ee))
- fix: the config prefix can be configurated, default is 'CONFIG_' ([dcad30c](https://gitlab.com/charlie-wong/kcbs/commit/dcad30c))